<?php

declare(strict_types=1);

namespace Smorken\IpAuth\User\Contracts\Repositories;

use Smorken\Domain\Repositories\Contracts\RetrieveRepository;

interface FindActiveRepository extends RetrieveRepository
{
    public function setIp(string $ip): self;
}
