<?php

declare(strict_types=1);

namespace Smorken\IpAuth\User\Contracts\Repositories;

use Smorken\Domain\Repositories\Contracts\FilteredRepository;

interface FilteredUsersRepository extends FilteredRepository {}
