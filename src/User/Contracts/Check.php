<?php

namespace Smorken\IpAuth\User\Contracts;

interface Check
{
    public function isValid(string|int $user_id, ...$params): bool;

    public function setBackend(mixed $handler): void;
}
