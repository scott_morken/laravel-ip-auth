<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 6/21/18
 * Time: 1:34 PM
 */

namespace Smorken\IpAuth\User\Contracts\Models;

interface User extends \Smorken\Auth\Contracts\Models\User {}
