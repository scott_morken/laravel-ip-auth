<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 6/21/18
 * Time: 7:33 AM
 */

namespace Smorken\IpAuth\User\Contracts\Models;

use Smorken\Model\Contracts\Model;

/**
 * Interface Active
 *
 *
 * @property string $ip
 * @property string $user_id
 *
 * @phpstan-require-extends \Smorken\IpAuth\User\Models\Eloquent\Active
 */
interface Active extends Model {}
