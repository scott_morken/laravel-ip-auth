<?php

declare(strict_types=1);

namespace Smorken\IpAuth\User\Contracts\Actions;

use Smorken\Domain\Actions\Contracts\Action;
use Smorken\IpAuth\Shared\Contracts\ValueObjects\IpAndAttributesValueObject;
use Smorken\IpAuth\User\Contracts\Models\Active;

interface CreateActiveUserAction extends Action
{
    public function __invoke(IpAndAttributesValueObject $vo): Active;
}
