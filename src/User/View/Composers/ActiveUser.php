<?php

namespace Smorken\IpAuth\User\View\Composers;

use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Smorken\IpAuth\User\Contracts\Repositories\FindActiveRepository;

class ActiveUser
{
    public function __construct(protected Request $request, protected FindActiveRepository $findActiveRepository) {}

    public function compose(View $view): void
    {
        $active = ($this->findActiveRepository)($this->request->ip());
        $view->with('activeUser', $active);
    }
}
