<?php

declare(strict_types=1);

namespace Smorken\IpAuth\User\Repositories;

use Smorken\Domain\Repositories\EloquentFilteredRepository;
use Smorken\IpAuth\User\Contracts\Models\User;

class FilteredUsersRepository extends EloquentFilteredRepository implements \Smorken\IpAuth\User\Contracts\Repositories\FilteredUsersRepository
{
    public function __construct(User $model)
    {
        parent::__construct($model);
    }
}
