<?php

declare(strict_types=1);

namespace Smorken\IpAuth\User\Repositories;

use Smorken\Domain\Repositories\EloquentRetrieveRepository;
use Smorken\IpAuth\User\Contracts\Models\Active;

class FindActiveRepository extends EloquentRetrieveRepository implements \Smorken\IpAuth\User\Contracts\Repositories\FindActiveRepository
{
    public function __construct(Active $model)
    {
        parent::__construct($model);
    }

    public function setIp(string $ip): \Smorken\IpAuth\User\Contracts\Repositories\FindActiveRepository
    {
        $this->setCacheKey($ip);

        return $this;
    }
}
