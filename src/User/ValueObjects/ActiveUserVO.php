<?php

declare(strict_types=1);

namespace Smorken\IpAuth\User\ValueObjects;

use Illuminate\Http\Request;
use Smorken\IpAuth\Shared\ValueObjects\IpAndAttributesValueObject;

class ActiveUserVO extends IpAndAttributesValueObject
{
    protected static function getAttributesFromRequest(Request $request): array
    {
        return [
            'user_id' => $request->input('user_id'),
        ];
    }
}
