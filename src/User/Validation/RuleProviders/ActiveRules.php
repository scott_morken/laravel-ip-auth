<?php

declare(strict_types=1);

namespace Smorken\IpAuth\User\Validation\RuleProviders;

class ActiveRules
{
    public static function rules(array $overrides = []): array
    {
        return [
            'ip' => 'required|ip',
            'user_id' => 'required|max:32',
            ...$overrides,
        ];
    }
}
