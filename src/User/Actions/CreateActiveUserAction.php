<?php

declare(strict_types=1);

namespace Smorken\IpAuth\User\Actions;

use Smorken\Domain\Actions\ActionWithEloquent;
use Smorken\IpAuth\Shared\Contracts\ValueObjects\IpAndAttributesValueObject;
use Smorken\IpAuth\User\Contracts\Models\Active;
use Smorken\IpAuth\User\Contracts\Repositories\FindActiveRepository;

class CreateActiveUserAction extends ActionWithEloquent implements \Smorken\IpAuth\User\Contracts\Actions\CreateActiveUserAction
{
    public function __construct(Active $model, protected FindActiveRepository $findActiveRepository)
    {
        parent::__construct($model);
    }

    public function __invoke(IpAndAttributesValueObject $vo): Active
    {
        $active = ($this->findActiveRepository)($vo->ip, false);
        if (! $active) {
            $active = $this->getModelInstance()->fill(['ip' => $vo->ip]);
        }
        $active->fill($vo->getUpdateAttributes());
        $active->save();

        return $active;
    }
}
