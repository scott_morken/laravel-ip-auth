<?php

namespace Smorken\IpAuth\User\Checks;

use Illuminate\Contracts\Config\Repository;
use Smorken\IpAuth\User\Contracts\Check;

class Role extends Base implements Check
{
    public function __construct(\Smorken\Roles\Contracts\Role $handler, protected Repository $config)
    {
        parent::__construct($handler);
    }

    public function isValid(string|int $user_id, ...$params): bool
    {
        $role = $this->config->get('ip-auth.routes.user_select.role', 100);

        return $this->handler->hasLevel($role, $user_id, false);
    }
}
