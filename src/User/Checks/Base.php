<?php

namespace Smorken\IpAuth\User\Checks;

use Smorken\IpAuth\User\Contracts\Check;

abstract class Base implements Check
{
    public function __construct(protected $handler) {}

    public function setBackend($handler): void
    {
        $this->handler = $handler;
    }
}
