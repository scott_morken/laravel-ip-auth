<?php

declare(strict_types=1);

namespace Smorken\IpAuth\User\Models\Builders;

use Smorken\Model\Filters\FilterHandler;
use Smorken\Model\QueryBuilders\Builder;

/**
 * @template TModel of \Smorken\IpAuth\User\Models\Eloquent\User
 *
 * @extends Builder<TModel>
 */
class UserBuilder extends \Smorken\Auth\Models\Builders\UserBuilder
{
    protected function getFilterHandlersForFilters(): array
    {
        return [
            new FilterHandler('roleIncludes', 'roleIncludes'),
        ];
    }
}
