<?php

declare(strict_types=1);

namespace Smorken\IpAuth\User\Models\Builders;

use Illuminate\Contracts\Database\Eloquent\Builder as EloquentBuilder;
use Smorken\Model\QueryBuilders\Builder;

/**
 * @template TModel of \Smorken\IpAuth\User\Models\Eloquent\Active
 *
 * @extends Builder<TModel>
 */
class ActiveBuilder extends Builder
{
    public function ipIs(string $ip): EloquentBuilder
    {
        /** @var $this */
        return $this->where('ip', '=', $ip);
    }

    public function userIdIs(string|int $userId): EloquentBuilder
    {
        /** @var $this */
        return $this->where('user_id', '=', $userId);
    }
}
