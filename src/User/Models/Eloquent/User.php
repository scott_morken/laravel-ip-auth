<?php

declare(strict_types=1);

namespace Smorken\IpAuth\User\Models\Eloquent;

use Illuminate\Database\Eloquent\HasBuilder;
use Smorken\IpAuth\User\Models\Builders\UserBuilder;

class User extends \Smorken\Auth\Models\Eloquent\User implements \Smorken\IpAuth\User\Contracts\Models\User
{
    /** @use HasBuilder<UserBuilder<static>> */
    use HasBuilder;

    protected static string $builder = UserBuilder::class;
}
