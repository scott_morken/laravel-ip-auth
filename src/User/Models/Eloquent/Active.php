<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 6/21/18
 * Time: 8:06 AM
 */

namespace Smorken\IpAuth\User\Models\Eloquent;

use Illuminate\Database\Eloquent\HasBuilder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Smorken\IpAuth\Shared\Models\Eloquent\Base;
use Smorken\IpAuth\User\Models\Builders\ActiveBuilder;
use Smorken\Model\Concerns\WithDefaultScopes;

class Active extends Base implements \Smorken\IpAuth\User\Contracts\Models\Active
{
    /** @use HasBuilder<ActiveBuilder<static>> */
    use HasBuilder;

    use WithDefaultScopes;

    public $incrementing = false;

    protected static string $builder = ActiveBuilder::class;

    protected $fillable = ['ip', 'user_id'];

    protected $primaryKey = 'ip';

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
