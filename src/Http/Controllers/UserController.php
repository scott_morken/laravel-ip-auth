<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 6/21/18
 * Time: 9:16 AM
 */

namespace Smorken\IpAuth\Http\Controllers;

use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;
use Smorken\Controller\View\Controller;
use Smorken\IpAuth\Shared\Constants\RedirectActionFor;
use Smorken\IpAuth\Shared\Contracts\Support\RedirectAction;
use Smorken\IpAuth\Shared\Contracts\ValueObjects\IpAndAttributesValueObject;
use Smorken\IpAuth\User\Contracts\Actions\CreateActiveUserAction;
use Smorken\IpAuth\User\Contracts\Repositories\FilteredUsersRepository;
use Smorken\IpAuth\User\Contracts\Repositories\FindActiveRepository;
use Smorken\IpAuth\User\ValueObjects\ActiveUserVO;
use Smorken\QueryStringFilter\Contracts\QueryStringFilter;
use Smorken\QueryStringFilter\Parts\Filter;

class UserController extends Controller
{
    protected string $baseView = 'sm-ip-auth::user';

    public function __construct(
        protected FilteredUsersRepository $filteredUsersRepository,
        protected FindActiveRepository $findActiveRepository,
        protected CreateActiveUserAction $createActiveUserAction,
        protected RedirectAction $redirectAction
    ) {
        parent::__construct();
    }

    public function __invoke(Request $request): View
    {
        $users = ($this->filteredUsersRepository)($this->getFilter($request), 0);
        $active = ($this->findActiveRepository)($request->ip(), false);

        return $this->makeView($this->getViewName('select'))
            ->with('users', $users)
            ->with('active_user', $active);
    }

    public function doSelect(Request $request): RedirectResponse
    {
        ($this->createActiveUserAction)($this->getValueObjectFromRequest($request));
        $this->findActiveRepository->setIp($request->ip())->reset();

        return Redirect::action($this->redirectAction->actionFor(RedirectActionFor::AFTER_SELECT_USER));
    }

    protected function getFilter(Request $request): QueryStringFilter
    {
        $filter = new Filter('roleIncludes');
        $filter->setValue($this->getRoleLevel());

        return \Smorken\QueryStringFilter\QueryStringFilter::from($request)
            ->setFilters([$filter])
            ->setHidden(['roleIncludes']);
    }

    protected function getRoleLevel(): int
    {
        return (int) Config::get('ip-auth.routes.user_select.role', 100);
    }

    protected function getValueObjectFromRequest(Request $request): IpAndAttributesValueObject
    {
        return ActiveUserVO::fromRequest($request);
    }
}
