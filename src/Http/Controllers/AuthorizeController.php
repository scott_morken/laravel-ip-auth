<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 6/21/18
 * Time: 8:15 AM
 */

namespace Smorken\IpAuth\Http\Controllers;

use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Smorken\Controller\Contracts\View\WithResource\WithDeleteAction;
use Smorken\Controller\Contracts\View\WithResource\WithFilteredRepository;
use Smorken\Controller\View\Controller;
use Smorken\Controller\View\WithResource\Concerns\HasDestroy;
use Smorken\Controller\View\WithResource\Concerns\HasFilteredIndex;
use Smorken\IpAuth\Ip\Contracts\Actions\CreateAuthorizedIpAction;
use Smorken\IpAuth\Ip\Contracts\Actions\DeleteIpAction;
use Smorken\IpAuth\Ip\Contracts\Repositories\FilteredIpsRepository;
use Smorken\IpAuth\Ip\ValueObjects\AuthorizeIpVO;
use Smorken\IpAuth\Shared\Constants\RedirectActionFor;
use Smorken\IpAuth\Shared\Contracts\Support\RedirectAction;
use Smorken\IpAuth\Shared\Contracts\ValueObjects\IpAndAttributesValueObject;
use Smorken\PinAuth\Auth\Contracts\Actions\LogoutAction;

class AuthorizeController extends Controller implements WithDeleteAction, WithFilteredRepository
{
    use HasDestroy, HasFilteredIndex {
        index as parentIndex;
    }

    protected string $baseView = 'sm-ip-auth::authorize';

    public function __construct(
        protected CreateAuthorizedIpAction $createAction,
        protected LogoutAction $logoutAction,
        protected RedirectAction $redirectAction,
        FilteredIpsRepository $filteredRepository,
        DeleteIpAction $deleteAction
    ) {
        parent::__construct();
        $this->filteredRepository = $filteredRepository;
        $this->deleteAction = $deleteAction;
    }

    public function doAuthorize(Request $request): RedirectResponse
    {
        ($this->createAction)($this->getValueObjectFromRequest($request));
        ($this->logoutAction)($request);

        return Redirect::action($this->redirectAction->actionFor(RedirectActionFor::AFTER_AUTHORIZE_IP));
    }

    public function index(Request $request): View
    {
        $v = $this->parentIndex($request);

        return $v->with('client_ip', $request->ip());
    }

    protected function getValueObjectFromRequest(Request $request): IpAndAttributesValueObject
    {
        return AuthorizeIpVO::fromRequest($request);
    }
}
