<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 6/21/18
 * Time: 7:38 AM
 */

namespace Smorken\IpAuth\Http\Middleware;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Smorken\IpAuth\Ip\Contracts\Repositories\FindIpByIpRepository;
use Smorken\IpAuth\Ip\Support\IpIsActive;

class IpActive
{
    public function __construct(protected FindIpByIpRepository $findIpRepository) {}

    public function handle(Request $request, \Closure $next)
    {
        if (! $this->isIpValid($request->ip())) {
            $msg = 'This computer has not been authorized.';
            $request->session()->flash('flash:danger', $msg);
            if ($request->wantsJson()) {
                return Response::json(['error' => $msg], 403);
            }

            return Redirect::route('ip-auth.authorize-ip');
        }

        return $next($request);
    }

    protected function isIpValid(string $ip): bool
    {
        $ipModel = ($this->findIpRepository)($ip);
        if (! $ipModel) {
            return false;
        }

        return IpIsActive::isActive($ipModel);
    }
}
