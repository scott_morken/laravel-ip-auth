<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 6/21/18
 * Time: 10:04 AM
 */

namespace Smorken\IpAuth\Http\Middleware;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Smorken\IpAuth\User\Contracts\Check;
use Smorken\IpAuth\User\Contracts\Repositories\FindActiveRepository;

class UserActive
{
    public function __construct(protected FindActiveRepository $findActiveRepository, protected Check $check) {}

    public function handle(Request $request, \Closure $next)
    {
        /** @var \Smorken\IpAuth\User\Contracts\Models\Active|null $active */
        $active = ($this->findActiveRepository)($request->ip(), false);
        if (! $active) {
            return $this->handleInactive($request);
        }
        if (! $this->check->isValid($active->user_id)) {
            $this->findActiveRepository->reset();

            return $this->redirectToSelect($request, 'Unauthorized user.');
        }

        return $next($request);
    }

    protected function handleInactive(Request $request): JsonResponse|RedirectResponse
    {
        $msg = 'Please select a user.';
        $request->session()->flash('flash:info', $msg);

        return $this->redirectToSelect($request, $msg);
    }

    protected function redirectToSelect(Request $request, string $msg): JsonResponse|RedirectResponse
    {
        if ($request->wantsJson()) {
            return Response::json(['error' => $msg], 403);
        }

        return Redirect::route('ip-auth.user-select');
    }
}
