<?php

declare(strict_types=1);

namespace Smorken\IpAuth\Shared\ValueObjects;

use Illuminate\Http\Request;

abstract class IpAndAttributesValueObject implements \Smorken\IpAuth\Shared\Contracts\ValueObjects\IpAndAttributesValueObject
{
    public function __construct(
        public string $ip,
        public array $attributes
    ) {}

    public static function fromRequest(
        Request $request
    ): self {
        return new static(
            $request->ip(),
            static::getAttributesFromRequest($request)
        );
    }

    abstract protected static function getAttributesFromRequest(Request $request): array;

    public function getUpdateAttributes(): array
    {
        return $this->attributes;
    }

    public function toArray(): array
    {
        return [
            'ip' => $this->ip,
            ...$this->attributes,
        ];
    }
}
