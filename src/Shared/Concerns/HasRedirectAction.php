<?php

declare(strict_types=1);

namespace Smorken\IpAuth\Shared\Concerns;

trait HasRedirectAction
{
    protected static string|array|null $redirectAction = null;

    abstract protected function getConfigKeyForRedirectAction(): string;

    public static function setRedirectAction(string|array $action): void
    {
        self::$redirectAction = $action;
    }

    protected function getRedirectAction(): string|array
    {
        if (self::$redirectAction) {
            return self::$redirectAction;
        }

        return $this->getRedirectActionFromConfig($this->getConfigKeyForRedirectAction());
    }

    protected function getRedirectActionFromConfig(string $key): string
    {
        return $this->getConfig()
            ->get($key, [\App\Http\Controllers\HomeController::class, 'index']);
    }
}
