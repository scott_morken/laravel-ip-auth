<?php

declare(strict_types=1);

namespace Smorken\IpAuth\Shared\Contracts\Support;

use Smorken\IpAuth\Shared\Constants\RedirectActionFor;

interface RedirectAction
{
    public function actionFor(RedirectActionFor $for): string|array;

    public static function setActionFor(RedirectActionFor $for, string|array $action): void;
}
