<?php

declare(strict_types=1);

namespace Smorken\IpAuth\Shared\Contracts\ValueObjects;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Request;

/**
 * @property string $ip
 * @property array $attributes
 *
 * @phpstan-require-extends \Smorken\IpAuth\Shared\ValueObjects\IpAndAttributesValueObject
 */
interface IpAndAttributesValueObject extends Arrayable
{
    public function __construct(
        string $ip,
        array $attributes
    );

    public function getUpdateAttributes(): array;

    public static function fromRequest(Request $request): self;
}
