<?php

declare(strict_types=1);

namespace Smorken\IpAuth\Shared\Constants;

enum RedirectActionFor: string
{
    case AFTER_AUTHORIZE_IP = 'after-authorize-ip';

    case AFTER_SELECT_USER = 'after-select-user';
}
