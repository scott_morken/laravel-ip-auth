<?php

namespace Smorken\IpAuth\Shared\View\Composers;

use Illuminate\Contracts\Config\Repository;
use Illuminate\Contracts\View\View;

class Times
{
    public function __construct(protected Repository $config) {}

    public function compose(View $view): void
    {
        $times = $this->config->get('ip-auth.times.data', []);
        $default_time = $this->config->get('ip-auth.times.default');
        $view->with('times', $times)
            ->with('default_time', $default_time);
    }
}
