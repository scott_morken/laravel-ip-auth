<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 6/21/18
 * Time: 8:07 AM
 */

namespace Smorken\IpAuth\Shared\Models\Eloquent;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Str;
use Smorken\Model\Eloquent;

abstract class Base extends Eloquent
{
    use HasFactory;

    protected static function newFactory()
    {
        $parts = Str::of(static::class)->explode('\\');
        $domain = $parts[2];
        $model = $parts->last();

        return App::make("Database\\Factories\\Smorken\\IpAuth\\Models\\Eloquent\\{$domain}\\{$model}Factory");
    }
}
