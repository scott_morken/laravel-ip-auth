<?php

declare(strict_types=1);

namespace Smorken\IpAuth\Shared\Support;

use Smorken\IpAuth\Shared\Constants\RedirectActionFor;

class RedirectAction implements \Smorken\IpAuth\Shared\Contracts\Support\RedirectAction
{
    protected static array $redirectActions = [];

    public function __construct(protected array $configActions) {}

    public static function setActionFor(RedirectActionFor $for, array|string $action): void
    {
        self::$redirectActions[$for->value] = $action;
    }

    public function actionFor(RedirectActionFor $for): string|array
    {
        return $this->getActionFromStaticArray($for) ?? $this->getActionFromConfig($for);
    }

    protected function getActionFromConfig(RedirectActionFor $for): string|array
    {
        return $this->configActions[$for->value];
    }

    protected function getActionFromStaticArray(RedirectActionFor $for): string|array|null
    {
        return self::$redirectActions[$for->value] ?? null;
    }
}
