<?php

namespace Smorken\IpAuth;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Validation\Factory;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\View;
use Smorken\IpAuth\Http\Middleware\IpActive;
use Smorken\IpAuth\Http\Middleware\UserActive;
use Smorken\IpAuth\Ip\Actions\CreateAuthorizedIpAction;
use Smorken\IpAuth\Ip\View\Composers\ClientIp;
use Smorken\IpAuth\Shared\Constants\RedirectActionFor;
use Smorken\IpAuth\Shared\Contracts\Support\RedirectAction;
use Smorken\IpAuth\Shared\View\Composers\Times;
use Smorken\IpAuth\User\Contracts\Check;
use Smorken\Roles\Contracts\Role;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    /**
     * Bootstrap the application events.
     */
    public function boot(): void
    {
        $this->bootConfig();
        $this->bootViews();
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        $this->loadRoutes();
        $this->aliasMiddleware();
        $this->addViewComposers();
        CreateAuthorizedIpAction::setValidationFactory($this->app[Factory::class]);
    }

    public function register(): void
    {
        $this->registerCheck();
        $this->registerRedirectActionProvider();
        $this->registerActions();
        $this->registerModels();
        $this->registerRepositories();
    }

    protected function addViewComposers(): void
    {
        $viewComposers = [
            ClientIp::class => [
                'sm-ip-auth::authorize.index',
            ],
            Times::class => [
                'sm-ip-auth::authorize.index',
            ],
        ];
        foreach ($viewComposers as $className => $views) {
            View::composer($views, $className);
        }
    }

    protected function aliasMiddleware(): void
    {
        $router = $this->app->make(Router::class);
        $router->aliasMiddleware('ip-active', IpActive::class);
        $router->aliasMiddleware('user-active', UserActive::class);
    }

    protected function bootConfig(): void
    {
        $config = __DIR__.'/../config/config.php';
        $this->mergeConfigFrom($config, 'ip-auth');
        $this->publishes([$config => config_path('ip-auth.php')], 'ip-auth-config');
    }

    protected function bootViews(): void
    {
        $this->loadViewsFrom(__DIR__.'/../views', 'sm-ip-auth');
        $this->publishes(
            [
                __DIR__.'/../views' => resource_path('/views/vendor/sm-ip-auth'),
            ],
            'ip-auth-views'
        );
    }

    protected function loadRoutes(): void
    {
        if ($this->app['config']->get('ip-auth.routes.load', true)) {
            $this->loadRoutesFrom(__DIR__.'/../routes/routes.php');
        }
    }

    protected function registerActions(): void
    {
        $this->booted(function (Application $app) {
            foreach ($app['config']->get('ip-auth.actions', []) as $contract => $impl) {
                $app->scoped($contract, static fn (Application $app) => $app[$impl]);
            }
        });
    }

    protected function registerCheck(): void
    {
        $this->app->bind(Check::class, function ($app) {
            $provider = $app['config']->get('ip-auth.check.provider', \Smorken\IpAuth\User\Checks\Role::class);
            $params = array_values($app['config']->get('ip-auth.check.params', ['handler' => Role::class]));

            return $app->make($provider, $params);
        });
    }

    protected function registerModels(): void
    {
        $this->booted(function (Application $app) {
            foreach ($app['config']->get('ip-auth.models', []) as $contract => $impl) {
                $app->scoped($contract, static fn (Application $app) => $app[$impl]);
            }
        });
    }

    protected function registerRedirectActionProvider(): void
    {
        $this->app->scoped(RedirectAction::class, function (Application $app) {
            $redirectAction = $app['config']->get(
                'ip-auth.routes.authorize.redirect',
                [\App\Http\Controllers\HomeController::class, 'index'] // @phpstan-ignore class.notFound
            );
            $redirectActions = [
                RedirectActionFor::AFTER_AUTHORIZE_IP->value => $redirectAction,
                RedirectActionFor::AFTER_SELECT_USER->value => $redirectAction,
            ];

            return new \Smorken\IpAuth\Shared\Support\RedirectAction($redirectActions);
        });
    }

    protected function registerRepositories(): void
    {
        $this->booted(function (Application $app) {
            foreach ($app['config']->get('ip-auth.repositories', []) as $contract => $impl) {
                $app->scoped($contract, static fn (Application $app) => $app[$impl]);
            }
        });
    }
}
