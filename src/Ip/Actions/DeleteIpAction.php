<?php

declare(strict_types=1);

namespace Smorken\IpAuth\Ip\Actions;

use Illuminate\Database\Eloquent\Model as EloquentModel;
use Smorken\Domain\Actions\EloquentDeleteAction;
use Smorken\IpAuth\Ip\Contracts\Models\Ip;
use Smorken\IpAuth\Ip\Contracts\Repositories\FindIpByIpRepository;
use Smorken\Model\Contracts\Model;

class DeleteIpAction extends EloquentDeleteAction implements \Smorken\IpAuth\Ip\Contracts\Actions\DeleteIpAction
{
    public function __construct(Ip $model, protected FindIpByIpRepository $findIpRepository)
    {
        parent::__construct($model);
    }

    protected function preDeleteModelActions(EloquentModel|Model $model): void
    {
        /** @var Ip $model */
        $this->findIpRepository->setIp($model->ip)->reset();
    }
}
