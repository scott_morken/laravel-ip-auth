<?php

declare(strict_types=1);

namespace Smorken\IpAuth\Ip\Actions;

use Smorken\Domain\Actions\ActionWithEloquent;
use Smorken\Domain\Actions\Concerns\HasValidationFactory;
use Smorken\IpAuth\Ip\Contracts\Models\Ip;
use Smorken\IpAuth\Ip\Contracts\Repositories\FindIpByIpRepository;
use Smorken\IpAuth\Ip\Validation\RuleProviders\IpRules;
use Smorken\IpAuth\Shared\Contracts\ValueObjects\IpAndAttributesValueObject;

class CreateAuthorizedIpAction extends ActionWithEloquent implements \Smorken\IpAuth\Ip\Contracts\Actions\CreateAuthorizedIpAction
{
    use HasValidationFactory;

    protected string $rulesProvider = IpRules::class;

    public function __construct(Ip $model, protected FindIpByIpRepository $findIpRepository)
    {
        parent::__construct($model);
    }

    public function __invoke(IpAndAttributesValueObject $vo): Ip
    {
        $data = $vo->toArray();
        $this->validate($data, $this->getRules());
        $ip = ($this->findIpRepository)($vo->ip);
        $this->findIpRepository->reset();
        if (! $ip) {
            $ip = $this->getModelInstance()->fill(['ip' => $vo->ip]);
        }
        $ip->fill($vo->getUpdateAttributes());
        $ip->save();

        return $ip;
    }
}
