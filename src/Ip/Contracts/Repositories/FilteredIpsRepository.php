<?php

declare(strict_types=1);

namespace Smorken\IpAuth\Ip\Contracts\Repositories;

use Smorken\Domain\Repositories\Contracts\FilteredRepository;

interface FilteredIpsRepository extends FilteredRepository {}
