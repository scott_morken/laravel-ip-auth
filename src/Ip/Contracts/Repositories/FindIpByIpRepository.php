<?php

declare(strict_types=1);

namespace Smorken\IpAuth\Ip\Contracts\Repositories;

use Smorken\Domain\Repositories\Contracts\Repository;
use Smorken\IpAuth\Ip\Contracts\Models\Ip;

interface FindIpByIpRepository extends Repository
{
    public function __invoke(string $ip, bool $throw = false): ?Ip;

    public function setIp(string $ip): self;
}
