<?php

declare(strict_types=1);

namespace Smorken\IpAuth\Ip\Contracts\Actions;

use Smorken\Domain\Actions\Contracts\Action;
use Smorken\IpAuth\Ip\Contracts\Models\Ip;
use Smorken\IpAuth\Shared\Contracts\ValueObjects\IpAndAttributesValueObject;

interface CreateAuthorizedIpAction extends Action
{
    public function __invoke(IpAndAttributesValueObject $vo): Ip;
}
