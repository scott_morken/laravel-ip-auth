<?php

declare(strict_types=1);

namespace Smorken\IpAuth\Ip\Contracts\Actions;

use Smorken\Domain\Actions\Contracts\DeleteAction;

interface DeleteIpAction extends DeleteAction {}
