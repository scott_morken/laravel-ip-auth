<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 6/21/18
 * Time: 7:33 AM
 */

namespace Smorken\IpAuth\Ip\Contracts\Models;

use Smorken\Model\Contracts\Model;

/**
 * Interface Ip
 *
 *
 * @property int $id
 * @property string $ip
 * @property \Carbon\Carbon $valid_until
 * @property string $user_id
 *
 * @phpstan-require-extends \Smorken\IpAuth\Ip\Models\Eloquent\Ip
 */
interface Ip extends Model {}
