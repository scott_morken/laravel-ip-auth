<?php

declare(strict_types=1);

namespace Smorken\IpAuth\Ip\Validation\RuleProviders;

use Illuminate\Support\Facades\Config;
use Smorken\IpAuth\Ip\Support\VerifyIP;

class IpRules
{
    public static function rules(array $overrides = []): array
    {
        $times = array_keys(Config::get('ip-auth.times.data', []));
        $max_time = last($times) ?: '+7 days';
        $ranges = Config::get('ip-auth.masks', []);
        $verifier = new VerifyIP($ranges);

        return [
            'ip' => [
                'required',
                'ip',
                function ($attribute, $value, $fail) use ($verifier) {
                    if (! $verifier->verify($value)) {
                        $fail($attribute.' is not in the defined IP range(s).');
                    }
                },
            ],
            'valid_until' => ['required', 'date', 'after_or_equal:now', 'before_or_equal:'.$max_time],
            'user_id' => ['required', 'int'],
            ...$overrides,
        ];
    }
}
