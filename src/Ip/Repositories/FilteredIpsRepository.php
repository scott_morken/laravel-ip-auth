<?php

declare(strict_types=1);

namespace Smorken\IpAuth\Ip\Repositories;

use Smorken\Domain\Repositories\EloquentFilteredRepository;
use Smorken\IpAuth\Ip\Contracts\Models\Ip;

class FilteredIpsRepository extends EloquentFilteredRepository implements \Smorken\IpAuth\Ip\Contracts\Repositories\FilteredIpsRepository
{
    public function __construct(Ip $model)
    {
        parent::__construct($model);
    }
}
