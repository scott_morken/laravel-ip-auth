<?php

declare(strict_types=1);

namespace Smorken\IpAuth\Ip\Repositories;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Smorken\Domain\Cache\Constants\CacheType;
use Smorken\Domain\Repositories\Concerns\RepositoryFromEloquent;
use Smorken\Domain\Repositories\Repository;
use Smorken\IpAuth\Ip\Contracts\Models\Ip;

class FindIpByIpRepository extends Repository implements \Smorken\IpAuth\Ip\Contracts\Repositories\FindIpByIpRepository
{
    use RepositoryFromEloquent;

    protected CacheType $cacheType = CacheType::BOTH;

    public function __construct(protected Ip $model) {}

    public function __invoke(string $ip, bool $throw = false): ?Ip
    {
        $this->setCacheKey($ip);

        return $this->getCacheHandler()->remember(
            $ip,
            $this->getDefaultCacheTTL(),
            fn () => $this->findByIp($ip, $throw)
        );
    }

    public function setIp(string $ip): \Smorken\IpAuth\Ip\Contracts\Repositories\FindIpByIpRepository
    {
        $this->setCacheKey($ip);

        return $this;
    }

    protected function findByIp(string $ip, bool $throw): ?Ip
    {
        $this->addQueryCallback(fn (Builder $q) => $q->ipIs($ip)); // @phpstan-ignore method.notFound
        $query = $this->query();

        // @phpstan-ignore return.type
        return $throw ? $query->firstOrFail($this->getColumns()) : $query->first($this->getColumns());
    }
}
