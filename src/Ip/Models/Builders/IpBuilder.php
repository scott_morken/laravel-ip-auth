<?php

declare(strict_types=1);

namespace Smorken\IpAuth\Ip\Models\Builders;

use Carbon\Carbon;
use Illuminate\Contracts\Database\Eloquent\Builder as EloquentBuilder;
use Smorken\Model\QueryBuilders\Builder;

/**
 * @template TModel of \Smorken\IpAuth\Ip\Models\Eloquent\Ip
 *
 * @extends Builder<TModel>
 */
class IpBuilder extends Builder
{
    public function defaultOrder(): EloquentBuilder
    {
        /** @var $this */
        return $this->orderValidUntil();
    }

    public function ipIs(string $ip): EloquentBuilder
    {
        /** @var $this */
        return $this->where('ip', '=', $ip);
    }

    public function isValid(): EloquentBuilder
    {
        /** @var $this */
        return $this->where('valid_until', '>=', Carbon::now());
    }

    public function orderValidUntil(): EloquentBuilder
    {
        /** @var $this */
        return $this->orderBy('valid_until', 'desc');
    }
}
