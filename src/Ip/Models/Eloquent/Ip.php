<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 6/21/18
 * Time: 8:09 AM
 */

namespace Smorken\IpAuth\Ip\Models\Eloquent;

use Illuminate\Database\Eloquent\HasBuilder;
use Smorken\IpAuth\Ip\Models\Builders\IpBuilder;
use Smorken\IpAuth\Shared\Models\Eloquent\Base;

class Ip extends Base implements \Smorken\IpAuth\Ip\Contracts\Models\Ip
{
    /** HasBuilder<IpBuilder<static>> */
    use HasBuilder;

    protected static string $builder = IpBuilder::class;

    protected $casts = ['valid_until' => 'datetime'];

    protected $fillable = ['ip', 'valid_until', 'user_id'];
}
