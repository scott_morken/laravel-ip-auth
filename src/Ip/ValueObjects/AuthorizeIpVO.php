<?php

declare(strict_types=1);

namespace Smorken\IpAuth\Ip\ValueObjects;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Smorken\IpAuth\Shared\ValueObjects\IpAndAttributesValueObject;

class AuthorizeIpVO extends IpAndAttributesValueObject
{
    protected static function getAttributesFromRequest(Request $request): array
    {
        return [
            'user_id' => $request->user('pin_user')->id,
            'valid_until' => Carbon::parse($request->input('valid_until')),
        ];
    }
}
