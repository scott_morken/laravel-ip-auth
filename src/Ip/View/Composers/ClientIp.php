<?php

namespace Smorken\IpAuth\Ip\View\Composers;

use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class ClientIp
{
    public function __construct(protected Request $request) {}

    public function compose(View $view): void
    {
        $view->with('client_ip', $this->request->getClientIp());
    }
}
