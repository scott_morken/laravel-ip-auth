<?php

declare(strict_types=1);

namespace Smorken\IpAuth\Ip\Support;

class VerifyIP
{
    public function __construct(protected array $ranges) {}

    public function verify(string $ip): bool
    {
        if (! $this->ranges) {
            return true;
        }
        foreach ($this->ranges as $range) {
            if ($this->ipInRange($ip, $range)) {
                return true;
            }
        }

        return false;
    }

    protected function ipInRange(string $ip, string $range): bool
    {
        if (! strpos($range, '/')) {
            $range .= '/32';
        }
        [$range, $netmask] = explode('/', $range, 2);
        $range_decimal = ip2long($range);
        $ip_decimal = ip2long($ip);
        $wildcard_decimal = (2 ** (32 - (int) $netmask)) - 1;
        $netmask_decimal = ~$wildcard_decimal;

        return ($ip_decimal & $netmask_decimal) == ($range_decimal & $netmask_decimal);
    }
}
