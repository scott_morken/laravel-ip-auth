<?php

declare(strict_types=1);

namespace Smorken\IpAuth\Ip\Support;

use Carbon\Carbon;
use Smorken\IpAuth\Ip\Contracts\Models\Ip;

class IpIsActive
{
    public static function isActive(Ip $ip): bool
    {
        $now = Carbon::now();

        return $ip->valid_until >= $now;
    }
}
