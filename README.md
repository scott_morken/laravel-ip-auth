### Laravel IP Authorization

Contains some helpers for the basic IP Authorization.  Note that IP addresses
can be spoofed to some extent (mainly incorrect use of a proxy).

* Service provider should be auto loaded but if not:

* Add `\Smorken\IpAuth\ServiceProvider::class` to the `config/app.php` service providers

* If needed, publish the config and view files
    * `php artisan vendor:publish --provider="Smorken\IpAuth\ServiceProvider" --tag=views`
    * `php artisan vendor:publish --provider="Smorken\IpAuth\ServiceProvider" --tag=config`
    
* Add the middleware to your `App\Http\Kernel.php`

```
    /**
     * The application's route middleware.
     *
     * @var array
     */
    protected $routeMiddleware = [
        ...
        'ip-active'   => \Smorken\IpAuth\Http\Middleware\IpActive::class,
        'user-active' => \Smorken\IpAuth\Http\Middleware\UserActive::class,
        ...
    ];
```

* Add the middleware key to your routes that need it

```
Route:middleware(['ip-active', 'user-active'])
    ->group(function () {
        Route::get('/', 'HomeController@index');
        Route::get('/customers', 'HomeController@customers');
        Route::post('/cart/{customer_id}', 'CartController@doCart');
    });
```

* Add to `config/menus.php`

```
...
    'role-manage' => [
        [
            'name' => 'Authorize',
            'action' => [\Smorken\IpAuth\Http\Controllers\AuthorizeController::class, 'index'],
            'children' => [],
        ],
...
```
