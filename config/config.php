<?php

use Smorken\IpAuth\Http\Controllers\AuthorizeController;
use Smorken\IpAuth\Http\Controllers\UserController;
use Smorken\IpAuth\Http\Middleware\IpActive;
use Smorken\PinAuth\Http\Middleware\PinUserAuthenticate;

return [
    'layout' => 'layouts.app',
    'routes' => [
        'load' => env('IPAUTH_LOAD_ROUTES', true),
        'user_select' => [
            'middleware' => ['web', IpActive::class],
            'prefix' => null,
            'controller' => UserController::class,
            'redirect' => [\App\Http\Controllers\HomeController::class, 'index'],
            'role' => env('IPAUTH_USER_ROLE', 100), // Role "level" for user
        ],
        'authorize' => [
            'middleware' => ['web', PinUserAuthenticate::class],
            'prefix' => 'ip-auth',
            'controller' => AuthorizeController::class,
            'redirect' => [\App\Http\Controllers\HomeController::class, 'index'],
        ],
    ],
    'masks' => [

    ],
    'times' => [
        'default' => env('IPAUTH_DEFAULT', '+12 hours'),
        'data' => [
            '+1 hour' => '1 hour',
            '+2 hours' => '2 hours',
            '+4 hours' => '4 hours',
            '+8 hours' => '8 hours',
            '+12 hours' => '12 hours',
            '+16 hours' => '16 hours',
            '+1 day' => '1 day',
            '+2 days' => '2 days',
            '+5 days' => '5 days',
            '+7 days' => '7 days',
        ],
    ],
    'check' => [
        'provider' => \Smorken\IpAuth\User\Checks\Role::class,
        'params' => [
            'handler' => \Smorken\Roles\Contracts\Role::class,
            'config' => \Illuminate\Contracts\Config\Repository::class,
        ],
    ],
    'actions' => [
        \Smorken\IpAuth\Ip\Contracts\Actions\CreateAuthorizedIpAction::class => \Smorken\IpAuth\Ip\Actions\CreateAuthorizedIpAction::class,
        \Smorken\IpAuth\Ip\Contracts\Actions\DeleteIpAction::class => \Smorken\IpAuth\Ip\Actions\DeleteIpAction::class,

        \Smorken\IpAuth\User\Contracts\Actions\CreateActiveUserAction::class => \Smorken\IpAuth\User\Actions\CreateActiveUserAction::class,
    ],
    'models' => [
        \Smorken\IpAuth\Ip\Contracts\Models\Ip::class => \Smorken\IpAuth\Ip\Models\Eloquent\Ip::class,

        \Smorken\IpAuth\User\Contracts\Models\Active::class => \Smorken\IpAuth\User\Models\Eloquent\Active::class,
        \Smorken\IpAuth\User\Contracts\Models\User::class => \Smorken\IpAuth\User\Models\Eloquent\User::class,
    ],
    'repositories' => [
        \Smorken\IpAuth\Ip\Contracts\Repositories\FilteredIpsRepository::class => \Smorken\IpAuth\Ip\Repositories\FilteredIpsRepository::class,
        \Smorken\IpAuth\Ip\Contracts\Repositories\FindIpByIpRepository::class => \Smorken\IpAuth\Ip\Repositories\FindIpByIpRepository::class,

        \Smorken\IpAuth\User\Contracts\Repositories\FilteredUsersRepository::class => \Smorken\IpAuth\User\Repositories\FilteredUsersRepository::class,
        \Smorken\IpAuth\User\Contracts\Repositories\FindActiveRepository::class => \Smorken\IpAuth\User\Repositories\FindActiveRepository::class,
    ],
];
