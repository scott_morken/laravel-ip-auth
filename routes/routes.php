<?php

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Route;
use Smorken\IpAuth\Http\Controllers\AuthorizeController;
use Smorken\IpAuth\Http\Controllers\UserController;
use Smorken\IpAuth\Http\Middleware\IpActive;
use Smorken\PinAuth\Http\Middleware\PinUserAuthenticate;

Route::middleware(Config::get('ip-auth.routes.user_select.middleware',
    ['web', IpActive::class]))
    ->prefix(Config::get('ip-auth.routes.user_select.prefix', null))
    ->group(function () {
        $controller = Config::get('ip-auth.routes.user_select.controller',
            UserController::class);
        Route::get('/select-user', $controller)->name('ip-auth.user-select');
        Route::post('/select-user', [$controller, 'doSelect']);
    });

Route::middleware(Config::get('ip-auth.routes.authorize.middleware',
    ['web', PinUserAuthenticate::class]))
    ->prefix(Config::get('ip-auth.routes.authorize.prefix', 'ip-auth'))
    ->group(function () {
        $controller = Config::get('ip-auth.routes.authorize.controller',
            AuthorizeController::class);
        Route::get('/', [$controller, 'index'])->name('ip-auth.authorize-ip');
        Route::post('/', [$controller, 'doAuthorize']);
        Route::get('/delete/{id}', [$controller, 'destroy']);
    });
