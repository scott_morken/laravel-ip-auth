<?php

declare(strict_types=1);

namespace Tests\Smorken\IpAuth\Concerns;

use Smorken\IpAuth\Ip\Models\Eloquent\Ip;
use Smorken\IpAuth\User\Models\Eloquent\Active;
use Smorken\IpAuth\User\Models\Eloquent\User;
use Smorken\Roles\Models\Eloquent\RoleUser;

trait TruncatesTables
{
    protected function truncateTables(): self
    {
        foreach ([
            Active::class,
            Ip::class,
            User::class,
            RoleUser::class,
        ] as $model) {
            $model::truncate();
        }

        return $this;
    }
}
