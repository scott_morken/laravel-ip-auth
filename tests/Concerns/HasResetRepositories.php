<?php

declare(strict_types=1);

namespace Tests\Smorken\IpAuth\Concerns;

use Smorken\IpAuth\Ip\Contracts\Repositories\FindIpByIpRepository;
use Smorken\IpAuth\User\Contracts\Repositories\FindActiveRepository;
use Smorken\Roles\Contracts\Repositories\FindRoleUserByUserIdRepository;
use Smorken\Roles\Contracts\Role;

trait HasResetRepositories
{
    protected function resetIpRepos(string $ip): self
    {
        foreach ([FindIpByIpRepository::class, FindActiveRepository::class] as $i) {
            $o = $this->app[$i];
            $o->setIp($ip)->reset();
        }

        return $this;
    }

    protected function resetRepoByCacheKey(
        mixed $key,
        string|\Smorken\Domain\Repositories\Contracts\Repository $repo
    ): self {
        if (is_string($repo)) {
            $repo = $this->app[$repo];
        }
        $repo->setCacheKey($key);

        return $this;
    }

    protected function resetRepos(array $ips, array $userIds): self
    {
        foreach ($ips as $ip) {
            $this->resetIpRepos($ip);
        }

        foreach ($userIds as $userId) {
            $this->resetRepoByCacheKey($userId, FindRoleUserByUserIdRepository::class);
        }

        return $this;
    }

    protected function resetRoleUser(): self
    {
        /** @var Role $role */
        $role = $this->app[Role::class];
        $role->resetUser();

        return $this;
    }
}
