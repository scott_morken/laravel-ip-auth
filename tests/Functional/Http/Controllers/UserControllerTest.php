<?php

declare(strict_types=1);

namespace Tests\Smorken\IpAuth\Functional\Http\Controllers;

use Illuminate\Foundation\Testing\RefreshDatabase;
use PHPUnit\Framework\Attributes\Test;
use Smorken\Auth\Models\Eloquent\User;
use Smorken\IpAuth\Ip\Models\Eloquent\Ip;
use Smorken\IpAuth\User\Models\Eloquent\Active;
use Smorken\Roles\Models\Eloquent\RoleUser;
use Tests\Smorken\IpAuth\Concerns\HasResetRepositories;
use Tests\Smorken\IpAuth\Concerns\TruncatesTables;
use Tests\Smorken\IpAuth\TestCase;

class UserControllerTest extends TestCase
{
    use HasResetRepositories, RefreshDatabase, TruncatesTables;

    #[Test]
    public function it_redirects_to_user_select_when_no_user_is_active(): void
    {
        $ip = Ip::factory()->create(['ip' => '127.0.0.1']);
        $response = $this->get('/');
        $response->assertRedirect('/select-user');
        $this->resetIpRepos('127.0.0.1')
            ->resetRoleUser();
    }

    #[Test]
    public function it_redirects_to_user_select_with_invalid_active_user(): void
    {
        $ip = Ip::factory()->create(['ip' => '127.0.0.1']);
        $user = User::factory()->create(['id' => 1]);
        $active = Active::factory()->create(['ip' => '127.0.0.1', 'user_id' => $user->id]);
        $user2 = User::factory()->create(['id' => 2]);
        $roleUser = RoleUser::factory()->create(['user_id' => $user2->id, 'role_id' => 1]);
        $response = $this->get('/');
        $response->assertRedirect('/select-user');
        $response = $this->get('/select-user');
        $response->assertSee($user2->name());
        $this->resetRepos(['127.0.0.1'], [$user->id, $user2->id])
            ->resetRoleUser();
    }

    #[Test]
    public function it_selects_an_active_user(): void
    {
        $ip = Ip::factory()->create(['ip' => '127.0.0.1']);
        $user = User::factory()->create(['id' => 1]);
        $roleUser = RoleUser::factory()->create(['user_id' => $user->id, 'role_id' => 1]);
        $response = $this->get('/');
        $response->assertRedirect('/select-user');
        $response = $this->get('/select-user');
        $response->assertSee($user->name());
        $this->resetRepos(['127.0.0.1'], [$user->id])
            ->resetRoleUser();
        $response = $this->post('/select-user', ['user_id' => $user->id]);
        $response->assertRedirect('/');
        $response = $this->get('/');
        $response->assertSee('done!');
        $this->resetRepos(['127.0.0.1'], [$user->id])
            ->resetRoleUser();
    }

    #[Test]
    public function it_shows_no_users_found_when_no_users_exists(): void
    {
        $ip = Ip::factory()->create(['ip' => '127.0.0.1']);
        $response = $this->get('/select-user');
        $response->assertSee('No users found. Please add users with the required authorization.');
        $this->resetIpRepos('127.0.0.1')
            ->resetRoleUser();
    }

    #[Test]
    public function it_shows_protected_page_with_active_ip_and_user(): void
    {
        $ip = Ip::factory()->create(['ip' => '127.0.0.1']);
        $user = User::factory()->create(['id' => 1]);
        $roleUser = RoleUser::factory()->create(['user_id' => $user->id, 'role_id' => 1]);
        $active = Active::factory()->create(['ip' => '127.0.0.1', 'user_id' => $user->id]);
        $response = $this->get('/');
        $response->assertSee('done!');
        $this->resetRepos(['127.0.0.1'], [1])
            ->resetRoleUser();
    }

    protected function tearDown(): void
    {
        $this->truncateTables();
        parent::tearDown();
    }
}
