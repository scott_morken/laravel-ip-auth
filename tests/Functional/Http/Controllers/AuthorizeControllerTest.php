<?php

declare(strict_types=1);

namespace Tests\Smorken\IpAuth\Functional\Http\Controllers;

use Illuminate\Foundation\Testing\RefreshDatabase;
use PHPUnit\Framework\Attributes\Test;
use Smorken\Auth\Models\Eloquent\User;
use Smorken\IpAuth\Ip\Contracts\Repositories\FilteredIpsRepository;
use Smorken\IpAuth\Ip\Models\Eloquent\Ip;
use Smorken\IpAuth\User\Models\Eloquent\Active;
use Smorken\PinAuth\Shared\Models\Eloquent\PinUser;
use Smorken\Roles\Models\Eloquent\RoleUser;
use Tests\Smorken\IpAuth\Concerns\TruncatesTables;
use Tests\Smorken\IpAuth\TestCase;

class AuthorizeControllerTest extends TestCase
{
    use RefreshDatabase, TruncatesTables;

    #[Test]
    public function it_authorizes_ip_after_redirect_and_has_user_selected(): void
    {
        $pu = PinUser::factory()->create();
        $user = User::factory()->create(['id' => 1]);
        $roleUser = RoleUser::factory()->create(['user_id' => $user->id, 'role_id' => 1]);
        $active = Active::factory()->create(['ip' => '127.0.0.1', 'user_id' => $user->id]);
        $this->actingAs($pu, 'pin_user');
        $response = $this->get('/');
        $response->assertRedirect('/ip-auth');
        $response = $this->post('/ip-auth', ['valid_until' => '+1 day']);
        $response->assertRedirect('/');
        $response = $this->get('/');
        $response->assertSee('done!');
    }

    #[Test]
    public function it_authorizes_ip_after_redirect_and_shows_user_list(): void
    {
        $pu = PinUser::factory()->create();
        $user = User::factory()->create(['id' => 1]);
        $roleUser = RoleUser::factory()->create(['user_id' => $user->id, 'role_id' => 1]);
        $this->actingAs($pu, 'pin_user');
        $response = $this->get('/');
        $response->assertRedirect('/ip-auth');
        $response = $this->post('/ip-auth', ['valid_until' => '+1 day']);
        $response->assertRedirect('/');
        $response = $this->get('/');
        $response->assertRedirect('/select-user');
        $response = $this->get('/select-user');
        $response->assertSee('Select the active user');
    }

    #[Test]
    public function it_can_login_with_pin_user(): void
    {
        $pu = PinUser::factory()->create();
        $response = $this->get('/');
        $response->assertRedirect('/ip-auth');
        $response = $this->get('/ip-auth');
        $response->assertRedirect('/pin/login');
        $response = $this->post('/pin/login', ['password' => '12345678']);
        $response->assertRedirect('/ip-auth');
        $response = $this->post('/ip-auth', ['valid_until' => '+1 day']);
        $response->assertRedirect('/');
        $response = $this->get('/');
        $response->assertRedirect('/select-user');
        $response = $this->get('/select-user');
        $response->assertSee('Select the active user');
    }

    #[Test]
    public function it_deletes_ip(): void
    {
        $pu = PinUser::factory()->create();
        $ip = Ip::factory()->create(['ip' => '10.0.0.1']);
        $this->actingAs($pu, 'pin_user');
        $response = $this->get('/ip-auth');
        $response->assertSee('10.0.0.1');
        $response = $this->get('/ip-auth/delete/'.$ip->id);
        $response->assertRedirect('/ip-auth');
        ($this->app[FilteredIpsRepository::class])->reset();
        $response = $this->get('/ip-auth');
        $response->assertDontSee('10.0.0.1');
    }

    protected function tearDown(): void
    {
        $this->truncateTables();
        parent::tearDown();
    }
}
