<?php

namespace Tests\Smorken\IpAuth\Stubs\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class HomeController extends Controller
{
    public function index(Request $request): Response
    {
        return \Illuminate\Support\Facades\Response::make('done!');
    }
}
