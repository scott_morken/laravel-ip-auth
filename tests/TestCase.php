<?php

namespace Tests\Smorken\IpAuth;

use Database\Seeders\Smorken\Roles\Models\Eloquent\RoleSeeder;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Contracts\Console\Kernel;
use Illuminate\Support\Facades\Route;
use Smorken\Domain\ActionServiceProvider;
use Smorken\Domain\DomainServiceProvider;
use Smorken\Domain\RepositoryServiceProvider;
use Smorken\IpAuth\ServiceProvider;
use Tests\Smorken\IpAuth\Stubs\Http\Controllers\HomeController;

abstract class TestCase extends \Orchestra\Testbench\TestCase
{
    protected $baseUrl = 'http://localhost';

    protected bool $seed = true;

    protected string $seeder = RoleSeeder::class;

    //    public function createApplication()
    //    {
    //        $app = require __DIR__.'/../vendor/orchestra/testbench-core/laravel/bootstrap/app.php';
    //        $app->make(Kernel::class)->bootstrap();
    //
    //        return $app;
    //    }

    protected function getEnvironmentSetup($app): void
    {
        tap($app['config'], function (Repository $config) {
            $config->set('app.env', 'production');
            $config->set('app.debug', false);
            $config->set('mail.driver', 'log');
            $config->set('database.default', 'testbench');
            $config->set(
                'database.connections.testbench',
                [
                    'driver' => 'sqlite',
                    'database' => ':memory:',
                    'prefix' => '',
                ]
            );
            $config->set('view.paths', [__DIR__.'/../views']);
            $config->set('ip-auth.routes.user_select.redirect', [HomeController::class, 'index']);
            $config->set('ip-auth.routes.authorize.redirect', [HomeController::class, 'index']);
            $config->set('auth.providers.pin_user_provider', ['driver' => 'pin_users']);
            $config->set('auth.guards.pin_user', ['driver' => 'session', 'provider' => 'pin_user_provider']);
            $config->set('sm-pin-auth.hasher_salt', 'abc12345def');
            $config->set('sm-pin-auth.layout', 'sm-ip-auth::layout');
            $config->set('ip-auth.layout', 'sm-ip-auth::layout');
        });
        Route::get('/', [HomeController::class, 'index'])->middleware(['web', 'ip-active', 'user-active']);
    }

    protected function getPackageProviders($app): array
    {
        return [
            ServiceProvider::class,
            \Smorken\CacheAssist\ServiceProvider::class,
            \Smorken\Auth\ServiceProvider::class,
            \Smorken\Roles\ServiceProvider::class,
            \Smorken\PinAuth\ServiceProvider::class,
            DomainServiceProvider::class,
            ActionServiceProvider::class,
            RepositoryServiceProvider::class,
        ];
    }
}
