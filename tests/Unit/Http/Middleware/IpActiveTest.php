<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 6/21/18
 * Time: 10:27 AM
 */

namespace Tests\Smorken\IpAuth\Unit\Http\Middleware;

use Carbon\Carbon;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Mockery as m;
use Smorken\IpAuth\Http\Middleware\IpActive;
use Smorken\IpAuth\Ip\Contracts\Repositories\FindIpByIpRepository;
use Smorken\IpAuth\Ip\Models\Eloquent\Ip;
use Tests\Smorken\IpAuth\TestCase;

class IpActiveTest extends TestCase
{
    protected ?FindIpByIpRepository $findIpByIpRepository = null;

    public function testExpiredNotJson(): void
    {
        $sut = $this->getSut();
        $this->getFindIpRepository()->expects()
            ->__invoke('1.1.1.1')
            ->andReturns(new Ip(['valid_until' => Carbon::now()->subDay()]));
        $rq = $this->getRequest();
        $rq->shouldReceive('session->flash')
            ->once()
            ->with('flash:danger', 'This computer has not been authorized.');
        $rq->shouldReceive('wantsJson')->andReturn(false);
        $redirResponse = m::mock(RedirectResponse::class);
        Redirect::shouldReceive('route')
            ->once()
            ->with('ip-auth.authorize-ip')
            ->andReturn($redirResponse);
        $this->assertEquals(
            $redirResponse,
            $sut->handle(
                $rq,
                function () {
                    return 'next';
                }
            )
        );
    }

    public function testNotValidNotJson(): void
    {
        $sut = $this->getSut();
        $this->getFindIpRepository()->expects()
            ->__invoke('1.1.1.1')
            ->andReturns(null);
        $rq = $this->getRequest();
        $rq->shouldReceive('session->flash')
            ->once()
            ->with('flash:danger', 'This computer has not been authorized.');
        $rq->shouldReceive('wantsJson')->andReturn(false);
        $redirResponse = m::mock(RedirectResponse::class);
        Redirect::shouldReceive('route')
            ->once()
            ->with('ip-auth.authorize-ip')
            ->andReturn($redirResponse);
        $this->assertEquals(
            $redirResponse,
            $sut->handle(
                $rq,
                function () {
                    return 'next';
                }
            )
        );
    }

    public function testNotValidWantsJson(): void
    {
        $sut = $this->getSut();
        $this->getFindIpRepository()->expects()
            ->__invoke('1.1.1.1')
            ->andReturns(null);
        $rq = $this->getRequest();
        $rq->shouldReceive('session->flash')
            ->once()
            ->with('flash:danger', 'This computer has not been authorized.');
        $rq->shouldReceive('wantsJson')->andReturn(true);
        Response::shouldReceive('json')->once()->with(['error' => 'This computer has not been authorized.'],
            403)->andReturn('authorize');
        $this->assertEquals(
            'authorize',
            $sut->handle(
                $rq,
                function () {
                    return 'next';
                }
            )
        );
    }

    public function testValid(): void
    {
        $sut = $this->getSut();
        $this->getFindIpRepository()->expects()
            ->__invoke('1.1.1.1')
            ->andReturns(new Ip(['valid_until' => Carbon::now()->addDay()]));
        $this->assertEquals(
            'next',
            $sut->handle(
                $this->getRequest(),
                function () {
                    return 'next';
                }
            )
        );
    }

    protected function getFindIpRepository(): FindIpByIpRepository
    {
        return $this->findIpByIpRepository ?? $this->findIpByIpRepository = m::mock(FindIpByIpRepository::class);
    }

    protected function getRequest(string $ip = '1.1.1.1'): Request
    {
        $r = m::mock(Request::class);
        $r->expects()->ip()->andReturn($ip);

        return $r;
    }

    protected function getSut(): IpActive
    {
        return new IpActive($this->getFindIpRepository());
    }
}
