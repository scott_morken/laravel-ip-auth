<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 6/21/18
 * Time: 10:58 AM
 */

namespace Tests\Smorken\IpAuth\Unit\Http\Middleware;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Mockery as m;
use Smorken\IpAuth\Http\Middleware\UserActive;
use Smorken\IpAuth\User\Contracts\Check;
use Smorken\IpAuth\User\Contracts\Repositories\FindActiveRepository;
use Tests\Smorken\IpAuth\TestCase;

class UserActiveTest extends TestCase
{
    protected ?Check $check = null;

    protected ?FindActiveRepository $findActiveRepository = null;

    public function testActive(): void
    {
        $a = new \Smorken\IpAuth\User\Models\Eloquent\Active(['user_id' => 1]);
        $this->getFindActiveRepository()
            ->expects()
            ->__invoke('1.1.1.1', false)
            ->andReturns($a);
        $this->getCheck()
            ->expects()
            ->isValid(1)
            ->andReturns(true);
        $sut = $this->getSut();
        $this->assertEquals(
            'next',
            $sut->handle(
                $this->getRequest(),
                function () {
                    return 'next';
                }
            )
        );
    }

    public function testActiveWithInvalidUserIsRedirectToSelect(): void
    {
        $a = new \Smorken\IpAuth\User\Models\Eloquent\Active(['user_id' => 1]);
        $this->getFindActiveRepository()
            ->expects()
            ->__invoke('1.1.1.1', false)
            ->andReturns($a);
        $this->getCheck()
            ->expects()
            ->isValid(1)
            ->andReturns(false);
        $this->getFindActiveRepository()
            ->expects()
            ->reset();
        $rq = $this->getRequest();
        $rq->shouldReceive('wantsJson')->andReturn(false);
        $response = m::mock(RedirectResponse::class);
        Redirect::shouldReceive('route')
            ->once()
            ->with('ip-auth.user-select')
            ->andReturn($response);
        $sut = $this->getSut();
        $this->assertEquals(
            $response,
            $sut->handle(
                $rq,
                function () {
                    return 'next';
                }
            )
        );
    }

    public function testNotActive(): void
    {
        $this->getFindActiveRepository()
            ->expects()
            ->__invoke('1.1.1.1', false)
            ->andReturns(null);
        $rq = $this->getRequest();
        $rq->shouldReceive('session->flash')
            ->once()
            ->with('flash:info', 'Please select a user.');
        $rq->shouldReceive('wantsJson')
            ->andReturn(false);
        $response = m::mock(RedirectResponse::class);
        Redirect::shouldReceive('route')
            ->once()
            ->with('ip-auth.user-select')
            ->andReturn($response);
        $sut = $this->getSut();
        $this->assertEquals(
            $response,
            $sut->handle(
                $rq,
                function () {
                    return 'next';
                }
            )
        );
    }

    public function testNotActiveJsonResponse(): void
    {
        $this->getFindActiveRepository()
            ->expects()
            ->__invoke('1.1.1.1', false)
            ->andReturns(null);
        $rq = $this->getRequest();
        $rq->shouldReceive('session->flash')
            ->once()
            ->with('flash:info', 'Please select a user.');
        $rq->shouldReceive('wantsJson')
            ->andReturn(true);
        $response = m::mock(JsonResponse::class);
        Response::shouldReceive('json')
            ->once()
            ->with(['error' => 'Please select a user.'], 403)
            ->andReturn($response);
        $sut = $this->getSut();
        $this->assertEquals(
            $response,
            $sut->handle(
                $rq,
                function () {
                    return 'next';
                }
            )
        );
    }

    protected function getCheck(): Check
    {
        return $this->check ?? $this->check = m::mock(Check::class);
    }

    protected function getFindActiveRepository(): FindActiveRepository
    {
        return $this->findActiveRepository ?? $this->findActiveRepository = m::mock(FindActiveRepository::class);
    }

    protected function getRequest(string $ip = '1.1.1.1'): Request
    {
        $r = m::mock(Request::class);
        $r->expects()->ip()->andReturns($ip);

        return $r;
    }

    protected function getSut(): UserActive
    {
        return new UserActive($this->getFindActiveRepository(), $this->getCheck());
    }
}
