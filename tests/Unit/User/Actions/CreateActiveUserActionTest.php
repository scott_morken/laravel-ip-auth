<?php

declare(strict_types=1);

namespace Tests\Smorken\IpAuth\Unit\User\Actions;

use Mockery as m;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Smorken\IpAuth\User\Actions\CreateActiveUserAction;
use Smorken\IpAuth\User\Contracts\Repositories\FindActiveRepository;
use Smorken\IpAuth\User\Models\Eloquent\Active;
use Smorken\IpAuth\User\ValueObjects\ActiveUserVO;
use Tests\Smorken\IpAuth\Concerns\WithMockConnection;

class CreateActiveUserActionTest extends TestCase
{
    use WithMockConnection;

    #[Test]
    public function it_creates_a_model(): void
    {
        $model = m::mock(new Active);
        $repo = m::mock(FindActiveRepository::class);
        $repo->expects()
            ->__invoke('127.0.0.1', false)
            ->andReturns(null);
        $a = m::mock(new Active);
        $model->expects()->newInstance()->andReturns($a);
        $a->expects()->fill(['ip' => '127.0.0.1'])->andReturnSelf();
        $a->expects()->fill(['user_id' => 1]);
        $a->expects()->save();
        $sut = new CreateActiveUserAction($model, $repo);
        $m = $sut(new ActiveUserVO('127.0.0.1', ['user_id' => 1]));
        $this->assertSame($a, $m);
    }

    #[Test]
    public function it_updates_an_existing_model(): void
    {
        $model = m::mock(new Active);
        $repo = m::mock(FindActiveRepository::class);
        $a = m::mock(\Smorken\IpAuth\User\Contracts\Models\Active::class);
        $repo->expects()
            ->__invoke('127.0.0.1', false)
            ->andReturns($a);
        $a->expects()->fill(['user_id' => 1]);
        $a->expects()->save();
        $sut = new CreateActiveUserAction($model, $repo);
        $m = $sut(new ActiveUserVO('127.0.0.1', ['user_id' => 1]));
        $this->assertSame($a, $m);
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->initConnectionResolver();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
