<?php

declare(strict_types=1);

namespace Tests\Smorken\IpAuth\Unit\User\Repositories;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Mockery as m;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Smorken\IpAuth\User\Models\Eloquent\Active;
use Smorken\IpAuth\User\Repositories\FindActiveRepository;
use Tests\Smorken\IpAuth\Concerns\WithCacheManager;

class FindActiveRepositoryTest extends TestCase
{
    use WithCacheManager;

    #[Test]
    public function it_finds_a_model(): void
    {
        $model = m::mock(new Active);
        $query = m::mock(Builder::class);
        $query->allows()->getModel()->andReturn($model);
        $model->expects()->newQuery()->andReturns($query);
        $query->expects()->defaultOrder()->andReturnSelf();
        $query->expects()->defaultWiths()->andReturnSelf();
        $query->expects()->select(['*'])->andReturnSelf();
        $m = new Active;
        $query->expects()->find('127.0.0.1')->andReturns($m);
        $sut = new FindActiveRepository($model);
        $ip = $sut('127.0.0.1', false);
        $this->assertSame($m, $ip);
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->initCacheAssist(true);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
