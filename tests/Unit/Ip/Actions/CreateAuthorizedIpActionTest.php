<?php

declare(strict_types=1);

namespace Tests\Smorken\IpAuth\Unit\Ip\Actions;

use Carbon\Carbon;
use Illuminate\Translation\ArrayLoader;
use Illuminate\Translation\Translator;
use Illuminate\Validation\Factory;
use Illuminate\Validation\ValidationException;
use Mockery as m;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Smorken\IpAuth\Ip\Actions\CreateAuthorizedIpAction;
use Smorken\IpAuth\Ip\Contracts\Repositories\FindIpByIpRepository;
use Smorken\IpAuth\Ip\Models\Eloquent\Ip;
use Smorken\IpAuth\Ip\ValueObjects\AuthorizeIpVO;
use Tests\Smorken\IpAuth\Concerns\WithApplication;
use Tests\Smorken\IpAuth\Concerns\WithMockConnection;

class CreateAuthorizedIpActionTest extends TestCase
{
    use WithApplication, WithMockConnection;

    #[Test]
    public function it_creates_a_model(): void
    {
        $this->setConfigRepository([
            'ip-auth' => [
                'times' => [
                    'data' => [
                        '+1 day' => '1 day',
                        '+2 days' => '2 days',
                        '+5 days' => '5 days',
                        '+7 days' => '7 days',
                    ],
                ],
                'masks' => [],
            ],
        ]);
        $model = m::mock(new Ip);
        $repo = m::mock(FindIpByIpRepository::class);
        $validUntil = Carbon::parse('+1 day');
        $repo->expects()
            ->__invoke('127.0.0.1')
            ->andReturns(null);
        $repo->expects()->reset();
        $ip = m::mock(new Ip);
        $model->expects()->newInstance()->andReturns($ip);
        $ip->expects()->fill(['ip' => '127.0.0.1'])->andReturnSelf();
        $ip->expects()->fill(['user_id' => 1, 'valid_until' => $validUntil]);
        $ip->expects()->save();
        $sut = new CreateAuthorizedIpAction($model, $repo);
        $m = $sut(new AuthorizeIpVO('127.0.0.1', ['user_id' => 1, 'valid_until' => $validUntil]));
        $this->assertSame($ip, $m);
    }

    #[Test]
    public function it_fails_validation(): void
    {
        $this->setConfigRepository([
            'ip-auth' => [
                'times' => [
                    'data' => [
                        '+1 day' => '1 day',
                        '+2 days' => '2 days',
                        '+5 days' => '5 days',
                        '+7 days' => '7 days',
                    ],
                ],
                'masks' => [],
            ],
        ]);
        $model = m::mock(new Ip);
        $repo = m::mock(FindIpByIpRepository::class);
        $sut = new CreateAuthorizedIpAction($model, $repo);
        $this->expectException(ValidationException::class);
        $this->expectExceptionMessage('validation.required (and 1 more error)');
        $sut(new AuthorizeIpVO('127.0.0.1', []));
    }

    #[Test]
    public function it_fails_validation_when_outside_of_subnet_mask(): void
    {
        $this->setConfigRepository([
            'ip-auth' => [
                'times' => [
                    'data' => [
                        '+1 day' => '1 day',
                        '+2 days' => '2 days',
                        '+5 days' => '5 days',
                        '+7 days' => '7 days',
                    ],
                ],
                'masks' => [
                    '10.0.0.1/24',
                ],
            ],
        ]);
        $model = m::mock(new Ip);
        $repo = m::mock(FindIpByIpRepository::class);
        $sut = new CreateAuthorizedIpAction($model, $repo);
        $this->expectException(ValidationException::class);
        $this->expectExceptionMessage('ip is not in the defined IP range(s).');
        $sut(new AuthorizeIpVO('127.0.0.1', ['user_id' => 1, 'valid_until' => '+1 day']));
    }

    #[Test]
    public function it_updates_an_existing_model(): void
    {
        $this->setConfigRepository([
            'ip-auth' => [
                'times' => [
                    'data' => [
                        '+1 day' => '1 day',
                        '+2 days' => '2 days',
                        '+5 days' => '5 days',
                        '+7 days' => '7 days',
                    ],
                ],
                'masks' => [],
            ],
        ]);
        $model = m::mock(new Ip);
        $repo = m::mock(FindIpByIpRepository::class);
        $ip = m::mock(\Smorken\IpAuth\Ip\Contracts\Models\Ip::class);
        $validUntil = Carbon::parse('+1 day');
        $repo->expects()
            ->__invoke('127.0.0.1')
            ->andReturns($ip);
        $repo->expects()->reset();
        $ip->expects()->fill(['user_id' => 1, 'valid_until' => $validUntil]);
        $ip->expects()->save();
        $sut = new CreateAuthorizedIpAction($model, $repo);
        $m = $sut(new AuthorizeIpVO('127.0.0.1', ['user_id' => 1, 'valid_until' => $validUntil]));
        $this->assertSame($ip, $m);
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->initConnectionResolver();
        CreateAuthorizedIpAction::setValidationFactory(new Factory(new Translator(new ArrayLoader, 'en')));
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
