<?php

declare(strict_types=1);

namespace Tests\Smorken\IpAuth\Unit\Ip\Support;

use PHPUnit\Framework\TestCase;
use Smorken\IpAuth\Ip\Support\VerifyIP;

class VerifyIPTest extends TestCase
{
    public function testDefaultNoRangeAllowsAnyIp(): void
    {
        $sut = new VerifyIP([]);
        $ip = '10.0.0.2';
        $this->assertTrue($sut->verify($ip));
    }

    public function testIpEqual(): void
    {
        $ranges = ['10.0.0.2'];
        $sut = new VerifyIP($ranges);
        $ip = '10.0.0.2';
        $this->assertTrue($sut->verify($ip));
    }

    public function testIpInOneOfRanges(): void
    {
        $ranges = ['10.0.0.2/24', '192.168.1.0/24'];
        $sut = new VerifyIP($ranges);
        $ip = '10.0.0.2';
        $this->assertTrue($sut->verify($ip));
    }

    public function testIpInRange(): void
    {
        $ranges = ['10.0.0.2/24'];
        $sut = new VerifyIP($ranges);
        $ip = '10.0.0.2';
        $this->assertTrue($sut->verify($ip));
    }

    public function testIpOutOfRanges(): void
    {
        $ranges = ['10.0.0.1', '10.0.1.0/24'];
        $sut = new VerifyIP($ranges);
        $ip = '10.0.0.2';
        $this->assertFalse($sut->verify($ip));
    }
}
