<?php

declare(strict_types=1);

namespace Tests\Smorken\IpAuth\Unit\Ip\Repositories;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Mockery as m;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Smorken\IpAuth\Ip\Models\Eloquent\Ip;
use Smorken\IpAuth\Ip\Repositories\FindIpByIpRepository;
use Tests\Smorken\IpAuth\Concerns\WithCacheManager;

class FindIpByIpRepositoryTest extends TestCase
{
    use WithCacheManager;

    #[Test]
    public function it_finds_a_model(): void
    {
        $model = m::mock(new Ip);
        $query = m::mock(Builder::class);
        $query->allows()->getModel()->andReturn($model);
        $model->expects()->newQuery()->andReturns($query);
        $query->expects()->ipIs('127.0.0.1')->andReturnSelf();
        $m = new Ip;
        $query->expects()->first(['*'])->andReturns($m);
        $sut = new FindIpByIpRepository($model);
        $ip = $sut('127.0.0.1');
        $this->assertSame($m, $ip);
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->initCacheAssist(true);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
