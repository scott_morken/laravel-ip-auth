<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $tables = [
            'actives',
            'ips',
        ];
        foreach ($tables as $table) {
            Schema::dropIfExists($table);
        }
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'actives',
            function (Blueprint $t) {
                $t->string('ip', 64);
                $t->string('user_id', 32);
                $t->timestamps();

                $t->primary('ip');
            }
        );

        Schema::create(
            'ips',
            function (Blueprint $t) {
                $t->increments('id');
                $t->string('ip', 128);
                $t->dateTime('valid_until');
                $t->string('user_id', 32);
                $t->timestamps();

                $t->index('ip', 'i_ip_ndx');
                $t->index('valid_until', 'i_valid_until_ndx');
            }
        );
    }
};
