<?php

namespace Database\Factories\Smorken\IpAuth\Models\Eloquent\Ip;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;
use Smorken\IpAuth\Ip\Models\Eloquent\Ip;

class IpFactory extends Factory
{
    protected $model = Ip::class;

    public function definition(): array
    {
        return [
            'ip' => $this->faker->ipv4,
            'valid_until' => Carbon::now()->addHours(4),
            'user_id' => 30000000,
        ];
    }
}
