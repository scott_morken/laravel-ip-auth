<?php

namespace Database\Factories\Smorken\IpAuth\Models\Eloquent\User;

use Illuminate\Database\Eloquent\Factories\Factory;
use Smorken\IpAuth\User\Models\Eloquent\Active;

class ActiveFactory extends Factory
{
    protected $model = Active::class;

    public function definition(): array
    {
        return [
            'ip' => $this->faker->ipv4,
            'user_id' => 30000000,
        ];
    }
}
