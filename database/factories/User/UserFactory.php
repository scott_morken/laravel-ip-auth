<?php

namespace Database\Factories\Smorken\IpAuth\Models\Eloquent\User;

use Illuminate\Database\Eloquent\Factories\Factory;
use Smorken\IpAuth\User\Models\Eloquent\User;

class UserFactory extends Factory
{
    protected static int $id = 30000000;

    protected $model = User::class;

    public function definition(): array
    {
        $subId = substr(self::$id, -5);
        $id = self::$id;
        $firstName = 'First'.$subId;
        $lastName = 'Last'.$subId;
        self::$id++;

        return [
            'id' => $id,
            'first_name' => $firstName,
            'last_name' => $lastName,
        ];
    }
}
