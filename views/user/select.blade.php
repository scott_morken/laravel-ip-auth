@php($layoutComponent = \Illuminate\Support\Facades\Config::get('ip-auth.layout', 'layouts.app'))
<x-dynamic-component :component="$layoutComponent">
    <h4>Select the active user</h4>
    @if ($users && count($users))
        <form method="post">
            @csrf
            <div class="form-group">
                <div class="row my-2">
                    @foreach ($users as $user)
                        <div class="col-sm-6 mb-2">
                            <button type="submit" name="user_id" value="{{ $user->id }}"
                                    class="btn btn-block btn-{{ $active_user && $active_user->user_id == $user->id ? 'success' : 'outline-primary'}} w-100">
                                {{ $user->name() }}
                            </button>
                        </div>
                    @endforeach
                </div>
            </div>
        </form>
    @else
        <div class="text-muted">No users found. Please add users with the required authorization.</div>
    @endif
</x-dynamic-component>
