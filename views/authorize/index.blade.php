<?php
/**
 * @var \Smorken\Domain\ViewModels\Contracts\FilteredViewModel $viewModel
 * @var \Illuminate\Support\Collection $models
 * @var \Smorken\IpAuth\Ip\Contracts\Models\Ip $model
 * @var string $client_ip
 */
$models = $viewModel->models();
?>
@php($layoutComponent = \Illuminate\Support\Facades\Config::get('ip-auth.layout', 'layouts.app'))
<x-dynamic-component :component="$layoutComponent"><h4>IPs Authorized</h4>
    <div class="card mb-2">
        <div class="card-body">
            <form method="post">
                @csrf
                <div class="row">
                    <div class="col">
                        <div class="form-control-plaintext">{{ $client_ip }}</div>
                    </div>
                    <div class="col">
                        <label for="valid_until" class="visually-hidden">Valid for</label>
                        <select name="valid_until" class="form-select">
                            @foreach ($times as $value => $text)
                                <option value="{{ $value }}" {{ $value === $default_time ? 'selected' : ''}}>{{ $text }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col">
                        <button type="submit" class="btn btn-success">Authorize</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    @if ($models && count($models))
        <table class="table table-striped">
            <thead>
            <tr>
                <th>ID</th>
                <th>IP Address</th>
                <th>Valid Until</th>
                <th>Updated By</th>
                <th>Last Updated</th>
                <th>&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($models as $model)
                <tr>
                    <td>{{ $model->id }}</td>
                    <td>{{ $model->ip }}</td>
                    <td>
                        {{ $model->valid_until }}
                        <small>({{ $model->valid_until->diffForHumans() }})</small>
                    </td>
                    <td>{{ $model->user_id }}</td>
                    <td>{{ $model->updated_at }}</td>
                    <td>
                        <a href="{{ action([$controller, 'destroy'], ['id' => $model->id]) }}"
                           title="Delete {{ $model->id }}">
                            Delete
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $models->links() }}
    @else
        <div class="text-muted">No records found.</div>
    @endif
</x-dynamic-component>>
